function checkScroll(){
    var startY = $('.navbar').height(); //The point where the navbar changes in px
    if($(window).scrollTop() > startY){
        $('.navbar').addClass("scrolled");
    }else{
        $('.navbar').removeClass("scrolled");
    }
}
function doVideoMute(){
	var startY = $('.cover-page').height();
	var sTop = $(window).scrollTop() + 100;
	if(sTop > startY){
		$('#background').prop('muted', true);
	} else {
		$('#background').prop('muted', false);
	}
}
$(window).scroll(function(){
	checkScroll();
	doVideoMute();
});
$(function() {
	$('a[href*="#"]:not([href="#"])').click(function() {
	  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    var target = $(this.hash);
	    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	    if (target.length) {
	      $('html, body').animate({
	        scrollTop: target.offset().top - 120
	      }, 1000);
	      return false;
	    }
	  }
	});
});
